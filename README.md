# Everyday Axum (a cargo-generate template)

This is an opinionated template to get you up and running with an Axum app. Some features:

- Askama for templates.
- Validator for form validation.
- An `AppError` enum deriving thiserror that knows how to 500 nicely.
- The Router is built up next to the relevant handlers.
- Handlers grouped by resource.
- A state that supports substates.
- A development server controlled via env with verbose logging.
- A production server that reads a config and writes to a socket.
- Cargo xtasks defined for project setup, running a dev server, and deployment.

## Use

This requires `cargo-generate`. To install that:

```sh
cargo install cargo-generate
```

Once you have that, using this template is done via:

```sh
cargo generate gl:mikeburns/everyday-axum-template
```

## Governance

By [Mike Burns]. The template is licensed under the [CC0 1.0]. The content
_generated_ by the template defaults to the GNU AGPL license.

[Mike Burns]: https://mike-burns.com/
[CC0 1.0]: https://creativecommons.org/publicdomain/zero/1.0/

I'll probably look at any issues and merge requests a lot a the beginning and
then taper off over time. This is a [SOFA]-inspired project, meant as a
foundation for my other SOFA-inspired projects. That means that it's going to
be pretty well maintained until I don't use it anymore.

[SOFA]: https://tilde.town/~dozens/sofa/
