use duct::cmd;

pub fn dev() -> std::io::Result<()> {
    cmd!("cargo", "run", "-p", "development").run().map(|_| ())
}
