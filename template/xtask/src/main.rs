mod deploy;
mod error;
mod generator;
mod server;
mod setup;
use crate::error::XtaskError;
use clap::{Args, Parser, Subcommand};

#[derive(Parser)]
#[command(author, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Command,
}

#[derive(Debug, Subcommand)]
enum Command {
    /// Push to a server
    Deploy,
    /// Run a local server
    #[command(alias = "s")]
    DevServer,
    /// Initialize app for development
    Setup,
    /// Generate code
    Generate(GeneratorArgs),
}

#[derive(Args, Debug)]
struct GeneratorArgs {
    /// Arbitrary args passed to the generator
    #[arg(num_args = 1.., allow_hyphen_values = true)]
    args: Vec<String>,
}

#[tokio::main]
async fn main() -> Result<(), XtaskError> {
    #[allow(unused_must_use)]
    {
        dotenvy::from_path(".env.local");
        dotenvy::from_path(".env");
    }

    let cli = Cli::parse();

    match &cli.command {
        Command::Setup => setup::setup()?,
        Command::DevServer => server::dev()?,
        Command::Deploy => deploy::deploy()?,
        Command::Generate(GeneratorArgs { args }) => generator::generate(&args)?,
    }

    Ok(())
}
