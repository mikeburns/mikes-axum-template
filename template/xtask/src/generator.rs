use duct::cmd;

pub fn generate(args: &[String]) -> std::io::Result<()> {
    #[allow(unreachable_code)] // for the todo!.
    if args.is_empty() {
        todo!("show a list of generators");
        // cargo lib
        // let config = Config::default()
        // Workspace::new(Path, &config)
        // https://github.com/bytecodealliance/cargo-component/blob/b8ba57ba0679df3b78de4f7a59516255243077c3/src/commands/add.rs#L76
        return Ok(());
    }

    let (name, specifics) = args.split_at(1);
    let prog = format!("generate-{}", name[0]);
    let mut args = vec![
        "run".to_owned(),
        "-p".to_owned(),
        "xtask".to_owned(),
        "-q".to_owned(),
        "--bin".to_owned(),
        prog,
    ];
    args.append(&mut specifics.to_vec());

    cmd("cargo", args).run().map(|_| Ok(()))?
}
