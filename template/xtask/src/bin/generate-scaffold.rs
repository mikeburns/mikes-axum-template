use askama::Template;
use clap::{Args, Parser};
use convert_case::{Case, Casing};
use std::collections::HashMap;
use std::convert::AsRef;
use std::fs;
use std::io::Write;
use std::path::Path;
use thiserror::Error;

#[derive(Parser)]
struct Config {
    /// The name of the resource
    // #[arg]
    resource: String,
    /// Fields on the resource
    #[command(flatten)]
    attributes: Attributes,
}

#[derive(Args)]
struct Attributes {
    /// field:type pairs
    pairings: Vec<String>,
}

// TODO logging
fn main() -> Result<(), ScaffoldError> {
    let config = Config::parse();

    generate_handler(&config)?;
    generate_model(&config)?;
    generate_views(&config)?;
    generate_templates(&config)?;
    generate_forms(&config)?;

    // TODO update lib.rs router() automatically.
    // TODO update state.rs automatically
    println!("You must now update app/src/lib.rs and app/src/state.rs");

    Ok(())
}

#[derive(Debug, Error)]
enum ScaffoldError {
    #[error("I/O error: {0}")]
    IoError(#[from] std::io::Error),
    #[error("template error: {0}")]
    AskamaError(#[from] askama::Error),
}

fn generate_handler(config: &Config) -> Result<(), ScaffoldError> {
    let resource_name = config.resource.clone();
    let resources_name = format!("{}s", resource_name.clone());
    let resource_upper_camel_case_name =
        resource_name.clone().to_case(Case::UpperCamel);
    let resources_upper_camel_case_name =
        resources_name.clone().to_case(Case::UpperCamel);
    let file_name = format!("app/src/handlers/{}.rs", resources_name.clone());
    let template = HandlerTemplate {
        resource_name,
        resources_name: resources_name.clone(),
        resource_upper_camel_case_name,
        resources_upper_camel_case_name,
    };

    create_file_from_template(file_name, template)?;
    append_file(
        "app/src/handlers/mod.rs",
        format!("pub mod {};\n", resources_name.clone()),
    )?;

    Ok(())
}

fn generate_model(config: &Config) -> Result<(), ScaffoldError> {
    let resource_name = config.resource.clone();
    let resources_name = format!("{}s", resource_name.clone());
    let resource_upper_camel_case_name =
        config.resource.clone().to_case(Case::UpperCamel);
    let resources_upper_camel_case_name =
        resources_name.clone().to_case(Case::UpperCamel);
    let mut fields = HashMap::with_capacity(config.attributes.pairings.len()); // TODO parse the pairing into a HashMap to begin with
    for pairing in &config.attributes.pairings {
        match pairing.split_once(":") {
            Some((name, t)) => {
                fields.insert(name.to_owned(), t.to_owned());
            }
            None => {
                fields.insert(pairing.clone(), "String".to_owned());
            }
        }
    }
    let template = ModelTemplate {
        resources_name,
        resource_upper_camel_case_name,
        resources_upper_camel_case_name,
        fields,
    };

    append_file_from_template("app/src/models.rs", template)?;

    Ok(())
}

fn generate_views(config: &Config) -> Result<(), ScaffoldError> {
    let resource_name = config.resource.clone();
    let resources_name = format!("{}s", resource_name.clone());
    let resource_upper_camel_case_name =
        resource_name.clone().to_case(Case::UpperCamel);
    let resources_upper_camel_case_name =
        resources_name.clone().to_case(Case::UpperCamel);
    let template = ViewsTemplate {
        resource_name,
        resources_name,
        resource_upper_camel_case_name,
        resources_upper_camel_case_name,
    };

    append_file_from_template("app/src/views.rs", template)?;

    Ok(())
}

fn generate_templates(config: &Config) -> Result<(), ScaffoldError> {
    let resource_name = config.resource.clone();
    let resources_name = format!("{}s", resource_name.clone());
    let mut fields = HashMap::with_capacity(config.attributes.pairings.len()); // TODO parse the pairing into a HashMap to begin with
    for pairing in &config.attributes.pairings {
        match pairing.split_once(":") {
            Some((name, t)) => {
                fields.insert(name.to_owned(), t.to_owned());
            }
            None => {
                fields.insert(pairing.clone(), "String".to_owned());
            }
        }
    }

    fs::create_dir_all(format!("app/templates/{}", resources_name.clone()))?;

    create_file_from_template(
        format!("app/templates/{}/index.html", resources_name.clone()),
        HtmlIndexTemplate {
            resource_name: resource_name.clone(),
            resources_name: resources_name.clone(),
        },
    )?;

    create_file_from_template(
        format!("app/templates/{}/new.html", resources_name.clone()),
        HtmlNewTemplate {
            resource_name: resource_name.clone(),
            resources_name: resources_name.clone(),
            fields: fields.clone(),
        },
    )?;

    create_file_from_template(
        format!("app/templates/{}/edit.html", resources_name.clone()),
        HtmlEditTemplate {
            resource_name: resource_name.clone(),
            resources_name: resources_name.clone(),
            fields: fields.clone(),
        },
    )?;

    create_file_from_template(
        format!("app/templates/{}/show.html", resources_name.clone()),
        HtmlShowTemplate {
            resource_name: resource_name.clone(),
            resources_name: resources_name.clone(),
            fields: fields.clone(),
        },
    )?;

    Ok(())
}

fn generate_forms(config: &Config) -> Result<(), ScaffoldError> {
    let resource_upper_camel_case_name =
        config.resource.clone().to_case(Case::UpperCamel);
    let mut fields = HashMap::with_capacity(config.attributes.pairings.len()); // TODO parse the pairing into a HashMap to begin with
    for pairing in &config.attributes.pairings {
        match pairing.split_once(":") {
            Some((name, t)) => {
                fields.insert(name.to_owned(), t.to_owned());
            }
            None => {
                fields.insert(pairing.clone(), "String".to_owned());
            }
        }
    }

    append_file_from_template(
        "app/src/forms.rs",
        FormTemplate {
            resource_upper_camel_case_name,
            fields,
        },
    )?;

    Ok(())
}

#[derive(Template)]
#[template(path = "scaffold/handler.rs", escape = "none")]
struct HandlerTemplate {
    resource_name: String,
    resources_name: String,
    resource_upper_camel_case_name: String,
    resources_upper_camel_case_name: String,
}

#[derive(Template)]
#[template(path = "scaffold/model.rs", escape = "none")]
struct ModelTemplate {
    resources_name: String,
    resource_upper_camel_case_name: String,
    resources_upper_camel_case_name: String,
    fields: HashMap<String, String>,
}

#[derive(Template)]
#[template(path = "scaffold/views.rs", escape = "none")]
struct ViewsTemplate {
    resource_name: String,
    resources_name: String,
    resource_upper_camel_case_name: String,
    resources_upper_camel_case_name: String,
}

#[derive(Template)]
#[template(path = "scaffold/index.html", escape = "none")]
struct HtmlIndexTemplate {
    resource_name: String,
    resources_name: String,
}

#[derive(Template)]
#[template(path = "scaffold/new.html", escape = "none")]
struct HtmlNewTemplate {
    resource_name: String,
    resources_name: String,
    fields: HashMap<String, String>,
}

#[derive(Template)]
#[template(path = "scaffold/edit.html", escape = "none")]
struct HtmlEditTemplate {
    resource_name: String,
    resources_name: String,
    fields: HashMap<String, String>,
}

#[derive(Template)]
#[template(path = "scaffold/show.html", escape = "none")]
struct HtmlShowTemplate {
    resource_name: String,
    resources_name: String,
    fields: HashMap<String, String>,
}

#[derive(Template)]
#[template(path = "scaffold/forms.rs", escape = "none")]
struct FormTemplate {
    resource_upper_camel_case_name: String,
    fields: HashMap<String, String>,
}

/// TODO move these elsewhere

fn create_file_from_template<P: AsRef<Path>>(
    path: P,
    template: impl Template,
) -> Result<(), ScaffoldError> {
    let contents = template.render()?;
    let mut file = fs::OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(path)?;
    write!(file, "{}", &contents)?;
    Ok(())
}

fn append_file_from_template<P: AsRef<Path>>(
    path: P,
    template: impl Template,
) -> Result<(), ScaffoldError> {
    let contents = template.render()?;
    let mut file = fs::OpenOptions::new()
        .write(true)
        .create(true)
        .append(true)
        .open(path)?;
    write!(file, "{}", &contents)?;
    Ok(())
}

fn append_file<P: AsRef<Path>, C: AsRef<[u8]>>(
    path: P,
    contents: C,
) -> Result<(), ScaffoldError> {
    let mut file = fs::OpenOptions::new().write(true).append(true).open(path)?;
    file.write_all(contents.as_ref())?;
    Ok(())
}

mod filters {
    pub fn add(s: &usize, n: usize) -> ::askama::Result<String> {
        Ok(format!("{}", s + n))
    }
}
