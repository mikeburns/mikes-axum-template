use thiserror::Error;

#[derive(Error, Debug)]
pub enum XtaskError {
    #[error("I/O error: {0}")]
    IoError(#[from] std::io::Error),
}
