#[derive(Template)]
#[template(path = "{{resources_name}}/index.html")]
pub struct {{resources_upper_camel_case_name}}IndexTemplate {
    pub state: AppState,
    pub {{resources_name}}: Vec<models::{{resource_upper_camel_case_name}}>,
}

#[derive(Template)]
#[template(path = "{{resources_name}}/show.html")]
pub struct {{resources_upper_camel_case_name}}ShowTemplate {
    pub state: AppState,
    pub {{resource_name}}: models::{{resource_upper_camel_case_name}},
    pub errors: HashMap<&'static str, validator::ValidationErrorsKind>,
}

#[derive(Template)]
#[template(path = "{{resources_name}}/new.html")]
pub struct {{resources_upper_camel_case_name}}NewTemplate {
    pub state: AppState,
    pub errors: HashMap<&'static str, validator::ValidationErrorsKind>,
}

#[derive(Template)]
#[template(path = "{{resources_name}}/edit.html")]
pub struct {{resources_upper_camel_case_name}}EditTemplate {
    pub state: AppState,
    pub {{resource_name}}: models::{{resource_upper_camel_case_name}},
    pub errors: HashMap<&'static str, validator::ValidationErrorsKind>,
}
