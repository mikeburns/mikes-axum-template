#[derive(Deserialize, Debug, Validate)]
pub struct {{resource_upper_camel_case_name}}CreateForm {
    {% for (name, t) in fields %}
    pub {{name}}: {{t}},{% endfor %}
}

#[derive(Deserialize, Debug, Validate)]
pub struct {{resource_upper_camel_case_name}}UpdateForm {
    pub method_: String,

    {% for (name, t) in fields %}
    pub {{name}}: {{t}},{% endfor %}
}
