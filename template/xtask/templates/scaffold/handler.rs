use crate::errors::AppError;
use crate::forms;
use crate::state::{AppState, DatabaseConnection};
use crate::views;
use crate::models;
use askama_axum::IntoResponse;
use axum::{
    extract::{Path, State},
    http::StatusCode,
    response::{self, Redirect, Response},
    routing::get,
    Form, Router,
};
use validator::Validate;
use uuid::Uuid;

pub fn router() -> Router<AppState> {
    Router::new()
        .route("/{{resources_name}}", get(index).post(create))
        .route("/{{resources_name}}/new", get(new))
        .route("/{{resources_name}}/:id", get(show).post(update).delete(destroy))
        .route("/{{resources_name}}/:id/edit", get(edit))
}

pub async fn index(
    State(db): State<DatabaseConnection>,
    State(state): State<AppState>,
) -> response::Result<views::{{resources_upper_camel_case_name}}IndexTemplate> {
    let {{resources_name}} = db.{{resources_name}}().all(20).await.map_err(Into::<AppError>::into)?;

    Ok(views::{{resources_upper_camel_case_name}}IndexTemplate { state, {{resources_name}} })
}

async fn show(
    State(db): State<DatabaseConnection>,
    State(state): State<AppState>,
    Path({{resource_name}}_id): Path<Uuid>,
) -> response::Result<views::{{resources_upper_camel_case_name}}ShowTemplate> {
    let {{resource_name}} = db.{{resources_name}}().get(&{{resource_name}}_id).await.map_err(Into::<AppError>::into)?;

    Ok(views::{{resources_upper_camel_case_name}}ShowTemplate {
        state,
        {{resource_name}},
        errors: validator::ValidationErrors::default().into_errors(),
    })
}

async fn new(
    State(state): State<AppState>,
) -> response::Result<views::{{resources_upper_camel_case_name}}NewTemplate> {
    Ok(views::{{resources_upper_camel_case_name}}NewTemplate {
        state,
        errors: validator::ValidationErrors::default().into_errors(),
    })
}

async fn create(
    State(db): State<DatabaseConnection>,
    State(state): State<AppState>,
    Form(form): Form<forms::{{resource_upper_camel_case_name}}CreateForm>,
) -> response::Result<Response> {
    match form.validate() {
        Err(errors) => {
            Ok((
                StatusCode::UNPROCESSABLE_ENTITY,
                views::{{resources_upper_camel_case_name}}NewTemplate {
                    state,
                    errors: errors.into_errors(),
                },
            )
                .into_response())
        }
        Ok(_) => {
            let {{resource_name}}: models::{{resource_upper_camel_case_name}} = db
                .{{resources_name}}()
                .create_from_form(&form)
                .await
                .map_err(Into::<AppError>::into)?;
            Ok(
                Redirect::to(&format!("{{resources_name}}/{}", {{resource_name}}.id))
                    .into_response(),
            )
        }
    }
}

async fn edit(
    State(db): State<DatabaseConnection>,
    State(state): State<AppState>,
    Path({{resource_name}}_id): Path<Uuid>,
) -> response::Result<views::{{resources_upper_camel_case_name}}EditTemplate> {
    let {{resource_name}} = db
        .{{resources_name}}()
        .get(&{{resource_name}}_id)
        .await
        .map_err(Into::<AppError>::into)?;

    Ok(views::{{resources_upper_camel_case_name}}EditTemplate {
        state,
        {{resource_name}},
        errors: validator::ValidationErrors::default().into_errors(),
    })
}

async fn update(
    State(db): State<DatabaseConnection>,
    State(state): State<AppState>,
    Path({{resource_name}}_id): Path<Uuid>,
    Form(form): Form<forms::{{resource_upper_camel_case_name}}UpdateForm>,
) -> response::Result<Response> {
    if form.method_ != "PATCH" {
        return Err(AppError::InvalidRequestError.into());
    }

    let {{resource_name}} = db
        .{{resources_name}}()
        .get(&{{resource_name}}_id)
        .await
        .map_err(Into::<AppError>::into)?;

    match form.validate() {
        Err(errors) => {
            Ok((
                StatusCode::UNPROCESSABLE_ENTITY,
                views::{{resources_upper_camel_case_name}}EditTemplate {
                    state,
                    {{resource_name}},
                    errors: errors.into_errors(),
                },
            )
                .into_response())
        }
        Ok(_) => {
            let {{resource_name}} = db
                .{{resources_name}}()
                .update_from_form(&{{resource_name}}_id, &form)
                .await
                .map_err(Into::<AppError>::into)?;
            Ok(Redirect::to(&format!("/{{resources_name}}/{}", {{resource_name}}.id)).into_response())
        }
    }
}

async fn destroy(
    State(db): State<DatabaseConnection>,
    State(_state): State<AppState>,
    Path({{resource_name}}_id): Path<Uuid>,
) -> response::Result<Response> {
    db.{{resources_name}}()
        .delete(&{{resource_name}}_id)
        .await
        .map_err(Into::<AppError>::into)?;
    Ok(Redirect::to("/{{resources_name}}/").into_response())
}
