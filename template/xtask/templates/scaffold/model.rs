use crate::forms;
use chrono::NaiveDateTime;
use sqlx::PgPool;
use uuid::Uuid;

#[derive(Debug)]
pub struct {{resource_upper_camel_case_name}} {
    {% for (name, t) in fields %}pub {{name}}: {{t}},
    {% endfor %}
}

pub struct {{resources_upper_camel_case_name}}Table(pub PgPool);

impl {{resources_upper_camel_case_name}}Table {
    pub async fn create_from_form(
        &self,
        form: &forms::{{resource_upper_camel_case_name}}CreateForm,
    ) -> Result<{{resource_upper_camel_case_name}}, sqlx::Error> {
        sqlx::query_as!(
            {{resource_upper_camel_case_name}},
                r#"
                  insert into {{resources_name}}
                  ({% for (name, _) in fields %}{{name}}{% if !loop.last %}, {% endif %}{% endfor %})
                  values
                  ({% for (_, _) in fields %}${{loop.index}}{% endfor %})
                  returning {% for (name, _) in fields %}{{name}}{% if !loop.last %}, {% endif %}{% endfor %}
                "#,
                {% for (name, _) in fields %}
                form.{{name}},
                {% endfor %}
            )
            .fetch_one(&self.0)
            .await
    }

    pub async fn update_from_form(
        &self,
        id: &Uuid,
        form: &forms::{{resource_upper_camel_case_name}}UpdateForm,
    ) -> Result<{{resource_upper_camel_case_name}}, sqlx::Error> {
        sqlx::query_as!(
            {{resource_upper_camel_case_name}},
                r#"
                  update {{resources_name}}
                  set
                  {% for (name, _) in fields %}
                  {% if name != "id" && name != "updated_at" -%}
                  {{name}} = ${{loop.index|add(1)}},
                  {% endif -%}
                  {% endfor %},
                  updated_at = now()
                  where id = $1
                  returning {% for (name, _) in fields %}{{name}}{% if !loop.last %}, {% endif %}{% endfor %}
                "#,
                id,
                {% for (name, _) in fields %}
                {% if name != "id" && name != "updated_at" -%}
                form.{{name}},
                {% endif -%}
                {% endfor %}
            )
            .fetch_one(&self.0)
            .await
    }

    pub async fn delete(
        &self,
        id: &Uuid,
    ) -> Result<sqlx::postgres::PgQueryResult, sqlx::Error> {
        sqlx::query!("delete from {{resources_name}} where id = $1", id)
            .execute(&self.0)
            .await
    }

    pub async fn all(&self, limit: i64) -> Result<Vec<{{resource_upper_camel_case_name}}>, sqlx::Error> {
        sqlx::query_as!(
            {{resource_upper_camel_case_name}},
            "select * from {{resources_name}} order by created_at desc limit $1",
            limit
        )
        .fetch_all(&self.0)
        .await
    }

    pub async fn get(&self, id: &Uuid) -> Result<{{resource_upper_camel_case_name}}, sqlx::Error> {
        sqlx::query_as!({{resource_upper_camel_case_name}}, "select * from {{resources_name}} where id = $1", id)
            .fetch_one(&self.0)
            .await
    }
}
