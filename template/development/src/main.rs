use std::env;

use app::state::{AppState, DatabaseConnection};
use sqlx::{
    postgres::{PgConnectOptions, PgPoolOptions},
    ConnectOptions,
};
use tower_http::trace::{DefaultMakeSpan, DefaultOnResponse, TraceLayer};
use tracing::{event, Level};
use tracing_subscriber::{fmt, EnvFilter};

#[tokio::main]
async fn main() {
    let format = fmt::format()
        .with_file(true)
        .with_line_number(true)
        .with_level(true)
        .pretty();
    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .event_format(format)
        .init();
    let trace_layer = TraceLayer::new_for_http()
        .make_span_with(DefaultMakeSpan::new().include_headers(true))
        .on_response(DefaultOnResponse::new().include_headers(true));

    let database_url = env::var("DATABASE_URL").expect("must set DATABASE_URL");
    let conn_options = database_url
        .parse::<PgConnectOptions>()
        .expect("failed to parse DATABASE_URL")
        .log_statements(log::LevelFilter::Debug)
        .clone();
    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect_with(conn_options)
        .await
        .expect("failed to connect to DB");

    let state = AppState {
        db: DatabaseConnection(pool),
    };

    #[rustfmt::skip]
    let router = app::router()
        .layer(trace_layer)
        .with_state(state);
    let server = axum::Server::bind(&"0.0.0.0:3001".parse().unwrap())
        .serve(router.into_make_service());

    event!(
        Level::INFO,
        "server is available: http://{}/",
        server.local_addr()
    );

    server.await.unwrap();
}
