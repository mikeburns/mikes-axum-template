# {{project-name}}

This is a Web app written in Rust using Axum.

## Setup

```sh
cargo xtask setup
```

## Verify the automated tests

```sh
cargo test
```

## Run locally

```sh
cargo xtask dev-server
```

## Deploy

```sh
cargo xtask deploy
```

## Generators

In order to speed development, this template ships with code generators. If you
follow the project layout conventions, the `scaffold` generator will help you
get up and running quickly.

For example, to generate an `Article` model with three fields, a CRUD
controller for it, and HTML templates and form objects for the controller:

```sh
cargo xtask generate scaffold article id:Uuid title:String body:String
```

Note that the generator does assume that every model has a unique identifier
field named `id`. It can hold any type of value but we expect it to hold an
`uuid::Uuid`.

## Governance

Copyright {{username}}. Released under the AGPL 3.0 as described in [LICENSE].

Interactions with the development team is subject to our [code of conduct].

[LICENSE]: ./LICENSE
[code of conduct]: ./CODE_OF_CONDUCT.md
