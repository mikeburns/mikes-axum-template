use crate::state::AppState;
use crate::views;
use axum::{extract::State, response, Router};

pub fn router() -> Router<AppState> {
    Router::new()
}

pub async fn index(
    State(state): State<AppState>,
) -> response::Result<views::DashboardsIndexTemplate> {
    Ok(views::DashboardsIndexTemplate { state })
}
