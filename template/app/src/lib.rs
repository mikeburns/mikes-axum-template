#![warn(clippy::pedantic)]
#![warn(
    clippy::pedantic,
    // clippy::suspicious_xor_used_as_pow,
    clippy::string_to_string,
    clippy::rc_mutex,
    clippy::rc_buffer,
    clippy::mem_forget,
    clippy::format_push_string,
    clippy::filetype_is_file,
    clippy::create_dir
)]
#![deny(
    clippy::wildcard_dependencies,
    clippy::unwrap_used,
    clippy::unneeded_field_pattern,
    clippy::unnecessary_self_imports,
    clippy::undocumented_unsafe_blocks,
    clippy::try_err,
    clippy::string_add,
    clippy::same_name_method,
    clippy::lossy_float_literal,
    clippy::float_cmp_const,
    clippy::exit
)]
#![allow(
    clippy::unused_async,
    clippy::missing_docs_in_private_items,
    clippy::implicit_return
)]

use axum::{routing::get, Router};
mod errors;
mod filters;
mod forms;
mod handlers;
mod models;
pub mod state;
mod views;
use crate::handlers::dashboards;
use crate::state::AppState;

pub fn router() -> Router<AppState> {
    Router::new()
        .route("/", get(dashboards::index))
        .merge(dashboards::router())
}
