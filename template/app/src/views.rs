#[allow(unused_imports)]
use crate::filters;
use crate::models;
use crate::state::AppState;
use askama::Template;
use std::collections::HashMap;

#[derive(Template)]
#[template(path = "dashboards/index.html")]
pub struct DashboardsIndexTemplate {
    pub state: AppState,
}
