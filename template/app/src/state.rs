use axum::extract::FromRef;
use sqlx::PgPool;

#[derive(Clone, FromRef)]
#[allow(clippy::module_name_repetitions)]
pub struct AppState {
    pub db: DatabaseConnection,
}

#[derive(Clone, FromRef)]
pub struct DatabaseConnection(pub PgPool);

impl DatabaseConnection {}
