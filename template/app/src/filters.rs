use chrono;
use std::collections::HashMap;

#[allow(clippy::unnecessary_wraps)]
#[allow(dead_code)]
pub fn field_errors(
    errors: &HashMap<&'static str, validator::ValidationErrorsKind>,
    name: &str,
) -> ::askama::Result<String> {
    if errors.contains_key(name) {
        let mut result = vec![];

        result.push(format!(r#"<div id="{}-errors">"#, name));

        match errors.get(name).expect("key somehow not in the errors") {
            validator::ValidationErrorsKind::Struct(_) => {
                result.push(
                    r#"<span class="error" role="alert">Unknown error</span>"#
                        .to_string(),
                );
            }
            validator::ValidationErrorsKind::List(_) => {
                result.push(
                    r#"<span class="error" role="alert">Unknown error</span>"#
                        .to_string(),
                );
            }
            validator::ValidationErrorsKind::Field(errs) => {
                for err in errs {
                    result.push(
                        format!(
                            r#"<span class="error" role="alert">{}</span>"#,
                            err
                        )
                        .to_string(),
                    );
                }
            }
        }

        result.push(r#"</div>"#.to_string());

        Ok(result.join(""))
    } else {
        Ok("".to_string())
    }
}

#[allow(dead_code)]
#[allow(clippy::unnecessary_wraps)]
pub fn input_errors(
    errors: &HashMap<&'static str, validator::ValidationErrorsKind>,
    name: &str,
) -> ::askama::Result<String> {
    if errors.contains_key(name) {
        Ok(format!(
            r#"aria-invalid="true" aria-error-message="{}-errors""#,
            name
        ))
    } else {
        Ok("".to_string())
    }
}

#[allow(dead_code)]
#[allow(clippy::unnecessary_wraps)]
pub fn or_blank<T>(maybe: &Option<T>) -> ::askama::Result<String>
where
    T: std::fmt::Display,
{
    match maybe {
        Some(t) => Ok(format!("{t}")),
        None => Ok(String::new()),
    }
}

#[allow(dead_code)]
#[allow(clippy::unnecessary_wraps)]
pub fn rfc3339(datetime: &chrono::NaiveDateTime) -> ::askama::Result<String> {
    Ok(format!("{}", datetime.format("%Y-%m-%dT%H:%M:%S")))
}
