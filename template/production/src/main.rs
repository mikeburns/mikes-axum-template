use app::state::{AppState, DatabaseConnection};
use futures::stream::StreamExt;
use hyperlocal::UnixServerExt;
use serde::Deserialize;
use signal_hook::consts;
use signal_hook_tokio::Signals;
use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
use std::{env::args, fs, path, path::PathBuf};
use tower_http::{
    classify::{ServerErrorsAsFailures, SharedClassifier},
    trace::TraceLayer,
};

#[tokio::main]
async fn main() {
    let config_file = args().nth(1).expect("must pass config file name");
    let config = read_config(config_file);
    let trace_layer = set_up_logging(&config);
    let state = make_state(&config).await;
    let sockpath = open_socket(&config);
    let (term_handle, term_tasks) = prepare_cleanup(sockpath.clone());

    #[rustfmt::skip]
    let router = app::router()
        .layer(trace_layer)
        .with_state(state);

    axum::Server::bind_unix(sockpath)
        .unwrap()
        .serve(router.into_make_service())
        .await
        .unwrap();

    term_handle.close();
    term_tasks.await.unwrap();
}

#[derive(Deserialize)]
struct Config {
    log_file: String,
    socket_file: String,
    rust_log: String,
    database_url: String,
}

async fn signal_handler(sockpath: PathBuf, mut signals: Signals) {
    while let Some(signal) = signals.next().await {
        if consts::TERM_SIGNALS.contains(&signal) {
            #[allow(unused_must_use)]
            {
                fs::remove_file(sockpath);
            }
            std::process::exit(0);
        }
    }
}

fn read_config<P: AsRef<path::Path>>(path: P) -> Config {
    let config_data = fs::read(path).expect("could not read config");
    toml::from_slice(&config_data).expect("could not parse config")
}

async fn make_state(config: &Config) -> AppState {
    let conn_options = config
        .database_url
        .parse::<PgConnectOptions>()
        .expect("failed to parse database_url")
        .clone();

    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect_with(conn_options)
        .await
        .expect("failed to connect to DB");

    AppState {
        db: DatabaseConnection(pool),
    }
}

fn set_up_logging(
    config: &Config,
) -> TraceLayer<SharedClassifier<ServerErrorsAsFailures>> {
    let log_file =
        fs::File::create(&config.log_file).expect("could not open log file");
    let format = tracing_subscriber::fmt::format()
        .with_file(false)
        .with_line_number(false)
        .with_level(true)
        .with_ansi(false)
        .compact();
    tracing_subscriber::fmt()
        .with_env_filter(tracing_subscriber::EnvFilter::new(&config.rust_log))
        .with_writer(log_file)
        .event_format(format)
        .init();
    TraceLayer::new_for_http()
}

fn open_socket(config: &Config) -> PathBuf {
    let sockpath = PathBuf::from(&config.socket_file);
    #[allow(unused_must_use)]
    {
        fs::remove_file(&sockpath);
    }

    sockpath
}

fn prepare_cleanup(
    sockpath: PathBuf,
) -> (signal_hook_tokio::Handle, tokio::task::JoinHandle<()>) {
    let term_signals = Signals::new(consts::TERM_SIGNALS)
        .expect("failed to track termination signals via unix pipe");
    let term_handle = term_signals.handle();
    let term_tasks = tokio::spawn(signal_handler(sockpath, term_signals));

    (term_handle, term_tasks)
}
